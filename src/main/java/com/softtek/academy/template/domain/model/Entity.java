package com.softtek.academy.template.domain.model;

import java.util.Date;

public class Entity {
	
	private long id;
	
	private String name;
	
	private Double money;
	
	private SubEntity entity;
	
//	@JsonSerialize(using = JsonDateTimeSerializer.class)
//    @JsonDeserialize(using = JsonDateTimeDeserializer.class)
	private Date date;

	public long getId() {
		return id;
	}

	public void setId(long id) {
		this.id = id;
	}

	public String getName() {
		return name;
	}

	public void setName(String name) {
		this.name = name;
	}

	public Double getMoney() {
		return money;
	}

	public void setMoney(Double money) {
		this.money = money;
	}

	public SubEntity getEntity() {
		return entity;
	}

	public void setEntity(SubEntity entity) {
		this.entity = entity;
	}

	public Date getDate() {
		return date;
	}

	public void setDate(Date date) {
		this.date = date;
	}

	@Override
	public String toString() {
		return "ModelEntity [id=" + id + ", name=" + name + ", money=" + money + ", entity=" + entity + ", date=" + date
				+ "]";
	}

}
