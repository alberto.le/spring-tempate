package com.softtek.academy.template.domain.dto;

public class Dto {
	
	private String name;

	public String getName() {
		return name;
	}

	public void setName(String name) {
		this.name = name;
	}

	@Override
	public String toString() {
		return "Dto [name=" + name + "]";
	}

}
