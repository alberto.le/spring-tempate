package com.softtek.academy.template.service.factory;

public interface Factory <T, K> {
	
	K factory(T t);

}
