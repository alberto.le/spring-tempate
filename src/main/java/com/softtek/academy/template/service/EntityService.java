package com.softtek.academy.template.service;

import java.util.ArrayList;
import java.util.List;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

import com.softtek.academy.template.domain.dto.Dto;
import com.softtek.academy.template.domain.model.Entity;
import com.softtek.academy.template.exception.InvalidInputException;
import com.softtek.academy.template.repository.EntityRepository;
import com.softtek.academy.template.service.factory.Factory;
import com.softtek.academy.template.service.validation.ObjectValidator;

@Service
public class EntityService {

	@Autowired
	private EntityRepository<Entity> entityRepository;
	
	@Autowired
	private Factory<Entity, Dto> dtoFactory;
	
	@Autowired
	private Factory<Dto, Entity> entityFactory;
	
	@Autowired
	private ObjectValidator<Dto> validator;
	
	public Dto getEntity(long id) {
		Entity entity = entityRepository.get(id);
		
		return dtoFactory.factory(entity);
	}
	
	public List<Dto> getAll() {
		List<Dto> dtos = new ArrayList<>();
		
		for (Entity entity : entityRepository.getAll()) {
			dtos.add(dtoFactory.factory(entity));
		}
	
		return dtos;
	}
	
	public Entity save(Dto dto) throws InvalidInputException {
		validator.validate(dto);
		
		Entity entity = entityFactory.factory(dto);
		return entityRepository.save(entity);
	}
	
}
