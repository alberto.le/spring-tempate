package com.softtek.academy.template.service.validation;

import org.apache.commons.lang3.StringUtils;
import org.springframework.stereotype.Component;

import com.softtek.academy.template.domain.dto.Dto;
import com.softtek.academy.template.exception.InvalidInputException;

@Component
public class EntityValidator implements ObjectValidator<Dto>{

	@Override
	public void validate(Dto dto) throws InvalidInputException {
	
		if ( StringUtils.isBlank(dto.getName()) ) {
			throw new InvalidInputException("Invalid name");
		}
		
	}

}
