package com.softtek.academy.template.service.factory;

import org.springframework.stereotype.Component;

import com.softtek.academy.template.domain.dto.Dto;
import com.softtek.academy.template.domain.model.Entity;

@Component
public class DtoFactory implements Factory<Entity, Dto> {

	@Override
	public Dto factory(Entity entity) {
		Dto dto = new Dto();
		dto.setName(entity.getName());
		
		return dto;
	}



}
