package com.softtek.academy.template.service.factory;

import java.util.Date;

import org.springframework.stereotype.Component;

import com.softtek.academy.template.domain.dto.Dto;
import com.softtek.academy.template.domain.model.Entity;
import com.softtek.academy.template.domain.model.SubEntity;

@Component
public class EntityFactory implements Factory<Dto, Entity>{

	@Override
	public Entity factory(Dto dto) {
		SubEntity subEntity = new SubEntity();
		subEntity.setId(0);
		
		Entity entity = new Entity();
		entity.setId(0);
		entity.setName(dto.getName());
		entity.setMoney(0.00);
		entity.setEntity(subEntity);
		entity.setDate(new Date());
		
		return entity;
	}

}
