package com.softtek.academy.template.service.validation;

import com.softtek.academy.template.exception.InvalidInputException;

public interface ObjectValidator<T> {

    void validate(final T object) throws InvalidInputException;

}
