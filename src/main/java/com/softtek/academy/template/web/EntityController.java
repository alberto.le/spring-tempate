package com.softtek.academy.template.web;

import java.util.List;

import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.PathVariable;
import org.springframework.web.bind.annotation.PostMapping;
import org.springframework.web.bind.annotation.RequestBody;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RestController;

import com.softtek.academy.template.domain.dto.Dto;
import com.softtek.academy.template.domain.model.Entity;
import com.softtek.academy.template.exception.InvalidInputException;
import com.softtek.academy.template.service.EntityService;

@RestController
@RequestMapping(path = "/entities")
public class EntityController {
	
	private static final Logger LOGGER = LoggerFactory.getLogger(EntityController.class);
	
	@Autowired
	private EntityService entityService;
	
	@GetMapping(path = "")
    public List<Dto> getEntities() {
    	LOGGER.info("Getting all entities");
    	
    	return entityService.getAll();
    }
	
	@PostMapping(path = "")
    public Entity save(@RequestBody Dto dto) throws InvalidInputException {
        LOGGER.info("Saving: {}", dto);

        return entityService.save(dto);
    }
	
	@GetMapping(path = "{entityId}")
    public Dto getDto(@PathVariable Long entityId) {
        LOGGER.info("Getting dto");
        
        return entityService.getEntity(entityId);
    }

}
