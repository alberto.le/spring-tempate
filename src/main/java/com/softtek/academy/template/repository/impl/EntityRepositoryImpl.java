package com.softtek.academy.template.repository.impl;

import java.util.List;

import org.springframework.beans.factory.ObjectFactory;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.jdbc.core.JdbcTemplate;
import org.springframework.stereotype.Repository;

import com.softtek.academy.template.domain.model.Entity;
import com.softtek.academy.template.repository.EntityRepository;
import com.softtek.academy.template.repository.mapper.EntityRowMapper;

@Repository
public class EntityRepositoryImpl implements EntityRepository<Entity> {
	
	private static final String SQL_SELECT = "SELECT * FROM TABLE ";

    @Autowired
    private JdbcTemplate jdbcTemplate;
    
    @Autowired
	private ObjectFactory<EntityRowMapper> rowMapperFactory;

	@Override
	public Entity get(long id) {
		return jdbcTemplate.queryForObject(SQL_SELECT + " WHERE id = ?", new Object[] {id}, rowMapperFactory.getObject());
	}

	@Override
	public List<Entity> getAll() {
		return jdbcTemplate.query(SQL_SELECT, rowMapperFactory.getObject());
	}

	@Override
	public Entity save(Entity entity) {
		final Object[] params = new Object[] {
	    		entity.getId(),
	    		entity.getName(),
	    		entity.getMoney(),
	    		entity.getEntity().getId(),
	    		entity.getDate()
	        };
	    	
	    	final String sql = "INSERT INTO [TABLE]([id],[name],[money],[sub_id],[date]) VALUES(?,?,?,?,?)";
	    	
	    	jdbcTemplate.update(sql, params);
	    	
	    	// Note here the new Object[] {} only has 1 parameter, change to params if you need them all. Also change the WHERE string to get all of params parameters 
	    	Entity returnEntity = jdbcTemplate.queryForObject(SQL_SELECT + " WHERE id = ?", new Object[] {entity.getId()}, rowMapperFactory.getObject());
	    	return returnEntity;
	}

	@Override
	public Entity update(Entity entity) {
		// TODO Auto-generated method stub
		return null;
	}

}
