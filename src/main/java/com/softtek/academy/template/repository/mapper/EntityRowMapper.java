package com.softtek.academy.template.repository.mapper;

import java.sql.ResultSet;
import java.sql.SQLException;

import org.springframework.jdbc.core.RowMapper;
import org.springframework.stereotype.Component;

import com.softtek.academy.template.domain.model.Entity;
import com.softtek.academy.template.domain.model.SubEntity;

@Component
public class EntityRowMapper implements RowMapper<Entity> {

	@Override
	public Entity mapRow(ResultSet rs, int rowNum) throws SQLException {
		SubEntity subEntity = new SubEntity();
		subEntity.setId(rs.getLong("sub_id"));
		subEntity.setName(rs.getString("sub_name"));
		
		Entity entity = new Entity();
		entity.setId(rs.getLong("id"));
		entity.setName(rs.getString("name"));
		entity.setMoney(rs.getDouble("money"));
		entity.setEntity(subEntity);
		entity.setDate(rs.getDate("date"));
		
		return entity;
	}

}
