package com.softtek.academy.template.repository;

import java.util.List;

public interface EntityRepository <T> {
	
	T get(long id);
	
	List<T> getAll();
	
	T save(T entity);
	
	T update(T entity);

}
